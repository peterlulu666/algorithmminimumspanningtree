import java.util.*;


public class Program2 {
    public class Vertex {
        private int row;
        private int column;
        private int intensity;
        private String vertexID;

        Vertex(int row, int column, int intensity) {
            this.row = row;
            this.column = column;
            this.intensity = intensity;
            this.vertexID = "[" + this.row + "]" + "[" + this.column + "]";
        }

        public int getRow() {
            return row;
        }

        public int getColumn() {
            return column;
        }

        public int getIntensity() {
            return intensity;
        }

        public String getVertexID() {
            return vertexID;
        }
    }

    public class Edge {
        private Vertex source;
        private Vertex destination;
        private int weight;
        private String destinationVertexID;

        Edge(Vertex source, Vertex destination) {
            this.source = source;
            this.destination = destination;
            this.weight = Math.abs(this.source.getIntensity() - this.destination.getIntensity());
            this.destinationVertexID = "[" + this.destination.getRow() + "]" + "[" + this.destination.getColumn() + "]";
        }

        public int getWeight() {
            return weight;
        }

        public String getDestinationVertexID() {
            return destinationVertexID;
        }
    }

    public int constructIntensityGraph(int[][] image) {
        int sumOfWeight = 0;
        int horizontalEdgeWeight = 0;
        int verticalEdgeWeight = 0;
        for (int row = 0; row < image.length; row++) {
            for (int column = 0; column < image[row].length; column++) {
                // Starting from the second row,
                // we will use the number in this row to subtract the number in the row – 1
                if (row >= 1 && row <= image.length - 1) {
                    // the difference is the distance and it is the edge weight
                    verticalEdgeWeight = Math.abs(image[row][column] - image[row - 1][column]);
                    // increment the sumOfWeight
                    sumOfWeight = sumOfWeight + verticalEdgeWeight;
                }

                // Starting from the second column,
                // we will use the number in this column to subtract the number in column – 1
                if (column >= 1 && column <= image[row].length - 1) {
                    // the difference is the distance and it is the edge weight
                    horizontalEdgeWeight = Math.abs(image[row][column] - image[row][column - 1]);
                    // increment the sumOfWeight
                    sumOfWeight = sumOfWeight + horizontalEdgeWeight;
                }
            }
        }

        return sumOfWeight;

    }

    public int constructPrunedGraph(int[][] image) {
        // store the graph in the HashMap
        // the key is the vertex and the value is the adjacent vertex and the edge weight
        // let's say that you have a graph like this
        // graph = {[0][3]={[0][2] =7, [0][4] =15, [1][3] =2},
        //          [0][4]={[0][3] =15, [1][4] =5},
        //          [0][1]={[0][0] =4, [0][2] =0, [1][1] =7},
        //          ...
        //          ...}
        // vertex[0][3] connects to [0][2] the edge weight is 7
        // vertex[0][3] connects to [0][4] the edge weight is 15
        // vertex[0][3] connects to [1][3] the edge weight is 2
        // ...
        // ...
        HashMap<String, HashMap<String, Integer>> graph = new HashMap<String, HashMap<String, Integer>>();

        for (int row = 0; row < image.length; row++) {
            for (int column = 0; column < image[row].length; column++) {
                Vertex vertex = new Vertex(row, column, image[row][column]);

                /*This code was taken from the website
                * Source code website:
                * https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/How-to-make-multiple-values-per-key-in-a-Java-map-possible
                *   Map<String, ArrayList<String>> multiValueMap = new HashMap<String, ArrayList<String>>();

                    multiValueMap.put("ford", new ArrayList<String>());
                    multiValueMap.get("ford").add("GT");
                    multiValueMap.get("ford").add("Mustang Mach-E");
                    multiValueMap.get("ford").add("Pantera");
                    *
                    * What I have learned:
                    * I learned how to make multiple values per key in a Java map possible
                * */
                // create the vertex and store the vertex in the HashMap
                // the vertexID is [x][y] and it is the key for the HashMap
                graph.put(vertex.getVertexID(), new HashMap<String, Integer>());
            }
        }

        // at this point, we have the vertex in the graph
        // so the graph is like this
        // {[0][3]={},
        // [0][4]={},
        // [0][1]={},
        // ...
        // ...}
//        System.out.println(graph);

        for (int row = 0; row < image.length; row++) {
            for (int column = 0; column < image[row].length; column++) {
                Vertex sourceVertex = new Vertex(row, column, image[row][column]);
                if (row >= 1) {
                    Vertex destinationVertex = new Vertex(row - 1, column, image[row - 1][column]);
                    Edge edge = new Edge(sourceVertex, destinationVertex);
                    // update adjacent vertex and the edge weight
                    graph.get(sourceVertex.getVertexID()).put(edge.getDestinationVertexID(), edge.getWeight());
                }
                if (row < image.length - 1) {
                    Vertex destinationVertex = new Vertex(row + 1, column, image[row + 1][column]);
                    Edge edge = new Edge(sourceVertex, destinationVertex);
                    // update adjacent vertex and the edge weight
                    graph.get(sourceVertex.getVertexID()).put(edge.getDestinationVertexID(), edge.getWeight());
                }
                if (column >= 1) {
                    Vertex destinationVertex = new Vertex(row, column - 1, image[row][column - 1]);
                    Edge edge = new Edge(sourceVertex, destinationVertex);
                    // update adjacent vertex and the edge weight
                    graph.get(sourceVertex.getVertexID()).put(edge.getDestinationVertexID(), edge.getWeight());
                }
                if (column < image[row].length - 1) {
                    Vertex destinationVertex = new Vertex(row, column + 1, image[row][column + 1]);
                    Edge edge = new Edge(sourceVertex, destinationVertex);
                    // update adjacent vertex and the edge weight
                    graph.get(sourceVertex.getVertexID()).put(edge.getDestinationVertexID(), edge.getWeight());
                }
            }
        }

        // at this point, we have the vertex, adjacent vertex, and the edge weight in the graph
        // so the graph is like this
        // {[0][3]={[0][4]=15, [0][2]=7, [1][3]=2},
        // [0][4]={[0][3]=15, [1][4]=5},
        // ...
        // ...}
//        System.out.println(graph);

        // since we have the graph, we will use prim's algorithm to find the minimum spanning tree

        /*infinity in Java
         * https://stackoverflow.com/questions/12952024/how-to-implement-infinity-in-java/17973014
         * */
        double infinity = Double.POSITIVE_INFINITY;
        HashMap<String, Integer> keys = new HashMap<String, Integer>();
        HashMap<String, String> parent = new HashMap<String, String>();
        /*cloning a HashSet
         * https://howtodoinjava.com/java/collections/hashmap/shallow-deep-copy-hashmap/
         * */
        /*unchecked cast warning when cloning a HashSet
         * https://stackoverflow.com/questions/9252803/how-to-avoid-unchecked-cast-warning-when-cloning-a-hashset
         * */
        @SuppressWarnings("unchecked")
        HashMap<String, HashMap<String, Integer>> unvisited = (HashMap<String, HashMap<String, Integer>>) graph.clone();

        /*iterate HashMap
         * https://www.geeksforgeeks.org/iterate-map-java/
         * */
        // let key to be infinity
        for (Map.Entry<String, HashMap<String, Integer>> entry : unvisited.entrySet()) {
            keys.put(entry.getKey(), (int) infinity);
        }

        // let parent to be null
        for (Map.Entry<String, HashMap<String, Integer>> entry : unvisited.entrySet()) {
            parent.put(entry.getKey(), null);
        }

        // let's select the vertex [0][0]
        String selecting_vertex = "[0][0]";

        // let the key to be 0
        keys.replace(selecting_vertex, 0);

        // while there exists vertex in the unvisited
        while (unvisited.size() != 0) {
            // find the vertex with the smallest key
            String minVertex = "";
            for (Map.Entry<String, HashMap<String, Integer>> entry : unvisited.entrySet()) {
                if (minVertex.equals("")) {
                    minVertex = entry.getKey();
                } else if (keys.get(entry.getKey()) < keys.get(minVertex)) {
                    minVertex = entry.getKey();
                }
            }

            // update parent and keys
            HashMap<String, Integer> adjacent_vertex = unvisited.get(minVertex);
            for (Map.Entry<String, Integer> entry : adjacent_vertex.entrySet()) {
                String child_vertex = entry.getKey();
                int weight = entry.getValue();
                if (unvisited.containsKey(child_vertex) && weight < keys.get(child_vertex)) {
                    parent.replace(child_vertex, minVertex);
                    keys.replace(child_vertex, weight);
                }
            }

            // remove the minVertex
            unvisited.remove(minVertex);

        }

        // print the minimum spanning tree
        for (Map.Entry<String, String> entry : parent.entrySet()) {
            System.out.println(entry.getValue() + " -- " + entry.getKey() + " edge weight is " + keys.get(entry.getKey()));
        }

        // print the sum of edge weight
        int sum = 0;
        for (Map.Entry<String, Integer> entry : keys.entrySet()) {
            int edgeWeight = entry.getValue();
            sum = sum + edgeWeight;
        }

        return sum;

    }
}















