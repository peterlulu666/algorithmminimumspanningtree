import java.util.*;

public class test {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // Creating Priority queue constructor having
        // initial capacity=5 and a StudentComparator instance
        // as its parameters
        PriorityQueue<Student> pq = new
                PriorityQueue<Student>(5, new StudentComparator());

        // Invoking a parameterized Student constructor with
        // name and cgpa as the elements of queue
        Student student1 = new Student("Nandini", 3.2);

        // Adding a student object containing fields
        // name and cgpa to priority queue
        pq.add(student1);
        Student student2 = new Student("Anmol", 3.6);
        pq.add(student2);
        Student student3 = new Student("Palak", 4.0);
        pq.add(student3);

        // Printing names of students in priority order,poll()
        // method is used to access the head element of queue
        System.out.println("Students served in their priority order");

        while (!pq.isEmpty()) {
            System.out.println(pq.poll().getName());
        }
    }
}

class StudentComparator implements Comparator<Student> {

    // Overriding compare()method of Comparator
    // for descending order of cgpa
    public int compare(Student s1, Student s2) {
        if (s1.cgpa < s2.cgpa)
            return 1;
        else if (s1.cgpa > s2.cgpa)
            return -1;
        return 0;
    }
}

class Student {
    public String name;
    public double cgpa;

    // A parameterized student constructor
    public Student(String name, double cgpa) {

        this.name = name;
        this.cgpa = cgpa;
    }

    public String getName() {
        return name;
    }
}

//        // Creating a priority queue
//        PriorityQueue<Integer> numbers = new PriorityQueue<>();
//        // Using the add() method
//        numbers.add(4);
//        numbers.add(2);
//        numbers.add(3);
//        numbers.add(1);
//        System.out.println("PriorityQueue: " + numbers);

//        // Using the offer() method
//        numbers.offer(1);
//        System.out.println("Updated PriorityQueue: " + numbers);
//        int a = 0;
//        int b = 1;
//        String str = "[" + a + "]" + "[" + b + "]";
//        String s = "[0][1]";
//        System.out.println(str);
//        System.out.println(s);
//        System.out.println(str.equals(s));

//        Map<String, ArrayList<String>> multiValueMap = new HashMap<String, ArrayList<String>>();
//        HashMap<String, HashMap<String, Integer>> outerMap = new HashMap<String, HashMap<String, Integer>>();
//
//
//        multiValueMap.put("ford", new ArrayList<String>());
//        multiValueMap.get("ford").add("GT");
//        multiValueMap.get("ford").add("Mustang Mach-E");
//        multiValueMap.get("ford").add("Pantera");
//        multiValueMap.get("ford").add("sport");
//
//        outerMap.put("Honda", new HashMap<String, Integer>());
//        outerMap.get("Honda").put("GT-p", 2);
//        outerMap.get("Honda").put("Mustang Mach-p", 2);
//        outerMap.get("Honda").put("Pantera-p", 3);
//        outerMap.get("Honda").put("sport-p", 6);
//
//
//        System.out.println(multiValueMap);
//        System.out.println(outerMap);




