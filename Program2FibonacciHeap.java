import java.util.*;


public class Program2FibonacciHeap {
    public class Vertex {
        private int row;
        private int column;
        private int intensity;
        private String vertexID;

        Vertex(int row, int column, int intensity) {
            this.row = row;
            this.column = column;
            this.intensity = intensity;
            this.vertexID = "[" + this.row + "]" + "[" + this.column + "]";
        }

        public int getRow() {
            return row;
        }

        public int getColumn() {
            return column;
        }

        public int getIntensity() {
            return intensity;
        }

        public String getVertexID() {
            return vertexID;
        }
    }

    public class Edge {
        private Vertex source;
        private Vertex destination;
        private int weight;
        private String destinationVertexID;

        Edge(Vertex source, Vertex destination) {
            this.source = source;
            this.destination = destination;
            this.weight = Math.abs(this.source.getIntensity() - this.destination.getIntensity());
            this.destinationVertexID = "[" + this.destination.getRow() + "]" + "[" + this.destination.getColumn() + "]";
        }

        public int getWeight() {
            return weight;
        }

        public String getDestinationVertexID() {
            return destinationVertexID;
        }
    }

    public class VertexKeyComparator implements Comparator<VertexKeyPair> {

        // Overriding compare()method of Comparator
        // for ascending order of keys
        // we will find the smallest key
        @Override
        public int compare(VertexKeyPair vertexKeyPair1, VertexKeyPair vertexKeyPair2) {
            if (vertexKeyPair1.keys > vertexKeyPair2.keys)
                return 1;
            else if (vertexKeyPair1.keys < vertexKeyPair2.keys)
                return -1;
            return 0;
        }
    }

    public class VertexKeyPair {
        public String vertex;
        public int keys;

        // A parameterized VertexKeyPair constructor
        public VertexKeyPair(String name, int keys) {
            this.vertex = name;
            this.keys = keys;
        }

        public String getVertex() {
            return vertex;
        }
    }

    public int constructIntensityGraph(int[][] image) {
        int sumOfWeight = 0;
        int horizontalEdgeWeight = 0;
        int verticalEdgeWeight = 0;
        for (int row = 0; row < image.length; row++) {
            for (int column = 0; column < image[row].length; column++) {
                // Starting from the second row,
                // we will use the number in this row to subtract the number in the row – 1
                if (row >= 1 && row <= image.length - 1) {
                    // the difference is the distance and it is the edge weight
                    verticalEdgeWeight = Math.abs(image[row][column] - image[row - 1][column]);
                    // increment the sumOfWeight
                    sumOfWeight = sumOfWeight + verticalEdgeWeight;
                }

                // Starting from the second column,
                // we will use the number in this column to subtract the number in column – 1
                if (column >= 1 && column <= image[row].length - 1) {
                    // the difference is the distance and it is the edge weight
                    horizontalEdgeWeight = Math.abs(image[row][column] - image[row][column - 1]);
                    // increment the sumOfWeight
                    sumOfWeight = sumOfWeight + horizontalEdgeWeight;
                }
            }
        }

        return sumOfWeight;

    }

    public int constructPrunedGraph(int[][] image) {
        // store the graph in the HashMap
        // the key is the vertex and the value is the adjacent vertex and the edge weight
        // let's say that you have a graph like this
        // graph = {[0][3]={[0][2] =7, [0][4] =15, [1][3] =2},
        //          [0][4]={[0][3] =15, [1][4] =5},
        //          [0][1]={[0][0] =4, [0][2] =0, [1][1] =7},
        //          ...
        //          ...}
        // vertex[0][3] connects to [0][2] the edge weight is 7
        // vertex[0][3] connects to [0][4] the edge weight is 15
        // vertex[0][3] connects to [1][3] the edge weight is 2
        // ...
        // ...
        HashMap<String, HashMap<String, Integer>> graph = new HashMap<String, HashMap<String, Integer>>();

        for (int row = 0; row < image.length; row++) {
            for (int column = 0; column < image[row].length; column++) {
                Vertex vertex = new Vertex(row, column, image[row][column]);

                /*This code was taken from the website
                * Source code website:
                * https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/How-to-make-multiple-values-per-key-in-a-Java-map-possible
                *   Map<String, ArrayList<String>> multiValueMap = new HashMap<String, ArrayList<String>>();

                    multiValueMap.put("ford", new ArrayList<String>());
                    multiValueMap.get("ford").add("GT");
                    multiValueMap.get("ford").add("Mustang Mach-E");
                    multiValueMap.get("ford").add("Pantera");
                    *
                    * What I have learned:
                    * I learned how to make multiple values per key in a Java map possible
                * */
                // create the vertex and store the vertex in the HashMap
                // the vertexID is [x][y] and it is the key for the HashMap
                graph.put(vertex.getVertexID(), new HashMap<String, Integer>());
            }
        }

        // at this point, we have the vertex in the graph
        // so the graph is like this
        // {[0][3]={},
        // [0][4]={},
        // [0][1]={},
        // ...
        // ...}
//        System.out.println(graph);

        for (int row = 0; row < image.length; row++) {
            for (int column = 0; column < image[row].length; column++) {
                Vertex sourceVertex = new Vertex(row, column, image[row][column]);
                if (row >= 1) {
                    Vertex destinationVertex = new Vertex(row - 1, column, image[row - 1][column]);
                    Edge edge = new Edge(sourceVertex, destinationVertex);
                    // update adjacent vertex and the edge weight
                    graph.get(sourceVertex.getVertexID()).put(edge.getDestinationVertexID(), edge.getWeight());
                }
                if (row < image.length - 1) {
                    Vertex destinationVertex = new Vertex(row + 1, column, image[row + 1][column]);
                    Edge edge = new Edge(sourceVertex, destinationVertex);
                    // update adjacent vertex and the edge weight
                    graph.get(sourceVertex.getVertexID()).put(edge.getDestinationVertexID(), edge.getWeight());
                }
                if (column >= 1) {
                    Vertex destinationVertex = new Vertex(row, column - 1, image[row][column - 1]);
                    Edge edge = new Edge(sourceVertex, destinationVertex);
                    // update adjacent vertex and the edge weight
                    graph.get(sourceVertex.getVertexID()).put(edge.getDestinationVertexID(), edge.getWeight());
                }
                if (column < image[row].length - 1) {
                    Vertex destinationVertex = new Vertex(row, column + 1, image[row][column + 1]);
                    Edge edge = new Edge(sourceVertex, destinationVertex);
                    // update adjacent vertex and the edge weight
                    graph.get(sourceVertex.getVertexID()).put(edge.getDestinationVertexID(), edge.getWeight());
                }
            }
        }

        // at this point, we have the vertex, adjacent vertex, and the edge weight in the graph
        // so the graph is like this
        // {[0][3]={[0][4]=15, [0][2]=7, [1][3]=2},
        // [0][4]={[0][3]=15, [1][4]=5},
        // ...
        // ...}
//        System.out.println(graph);

        // since we have the graph, we will use prim's algorithm to find the minimum spanning tree

        /*infinity in Java
         * https://stackoverflow.com/questions/12952024/how-to-implement-infinity-in-java/17973014
         * */
        // get the infinity
        double infinity = Double.POSITIVE_INFINITY;
        // store the minimum edge weight in keys
        HashMap<String, Integer> keys = new HashMap<String, Integer>();
        // store the parent vertex in parent
        HashMap<String, String> parent = new HashMap<String, String>();

        /*iterate HashMap
         * https://www.geeksforgeeks.org/iterate-map-java/
         * */
        // let key to be infinity
        for (Map.Entry<String, HashMap<String, Integer>> entry : graph.entrySet()) {
            keys.put(entry.getKey(), (int) infinity);
        }

        // let's select the vertex [0][0]
        String selecting_vertex = "[0][0]";

        // let the key to be 0
        keys.replace(selecting_vertex, 0);

        /*Implement PriorityQueue through Comparator in Java
         * https://www.geeksforgeeks.org/implement-priorityqueue-comparator-java/
         * https://howtodoinjava.com/java/collections/java-priorityqueue/
         * https://beginnersbook.com/2017/08/java-collections-priorityqueue-interface/
         * */
        // add tuple pair into the priority queue
        // initialize the capacity to be graph.size()
        // the pair is like (vertexID, key)
        PriorityQueue<VertexKeyPair> pq = new PriorityQueue<VertexKeyPair>(graph.size(), new VertexKeyComparator());
        for (Map.Entry<String, HashMap<String, Integer>> entry : graph.entrySet()) {
            String vertex = entry.getKey();
            pq.add(new VertexKeyPair(vertex, keys.get(vertex)));
        }

        // store visited vertex
        ArrayList<String> visited = new ArrayList<String>();

        // let parent to be null
        for (Map.Entry<String, HashMap<String, Integer>> entry : graph.entrySet()) {
            parent.put(entry.getKey(), null);
        }

        // while there exists vertex in the unvisited
        while (pq.size() != 0) {
            // poll a vertex with the smallest key
            VertexKeyPair minVertexPair = pq.poll();
            String minVertex = minVertexPair.getVertex();

            // add visited vertex to visited
            visited.add(minVertex);

            // update parent
            // update keys
            HashMap<String, Integer> adjacent_vertex = graph.get(minVertex);
            for (Map.Entry<String, Integer> entry : adjacent_vertex.entrySet()) {
                String child_vertex = entry.getKey();
                int weight = entry.getValue();
                // if the child_vertex is not in the visited and
                // the edge weight between the minVertex and the child_vertex is smaller than keys[child_vertex]
                if (!visited.contains(child_vertex) && weight < keys.get(child_vertex)) {
                    parent.replace(child_vertex, minVertex);
                    keys.replace(child_vertex, weight);
                }
            }

            // clear the priority queue
            pq.clear();

            // add all vertices that is not in visited into the priority queue
            for (Map.Entry<String, HashMap<String, Integer>> entry : graph.entrySet()) {
                String vertex = entry.getKey();
                if (!visited.contains(vertex)) {
                    pq.add(new VertexKeyPair(vertex, keys.get(vertex)));
                }
            }
        }

        // print the minimum spanning tree
        for (Map.Entry<String, String> entry : parent.entrySet()) {
            System.out.println(entry.getValue() + " -- " + entry.getKey() + " edge weight is " + keys.get(entry.getKey()));
        }

        // print the sum of edge weight
        int sum = 0;
        for (Map.Entry<String, Integer> entry : keys.entrySet()) {
            int edgeWeight = entry.getValue();
            sum = sum + edgeWeight;
        }

        return sum;

    }
}















